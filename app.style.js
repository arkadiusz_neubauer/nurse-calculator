import { StyleSheet } from 'react-native';

const margin = 20
const padding = 20

export default StyleSheet.create({
	scrollContainer: {
	},
	container: {
		flex: 1,
		justifyContent: 'center',
		alignItems: 'center',
		borderWidth: 0,
		borderRadius: 8,
		margin: margin,
		paddingLeft: padding,
		paddingRight: padding,
		paddingBottom: padding,
		backgroundColor: 'skyblue'
	},
	sectionResult: {
		flex: 1,
		justifyContent: 'center',
		alignItems: 'center',
		borderWidth: 0,
		borderRadius: 8,
		marginLeft: margin,
		marginRight: margin,
		padding: padding,  
		backgroundColor: '#B7DA9A'
	},
	input: {
		fontSize: 20,
		height: 40,
		borderWidth: 0,
		borderRadius: 8,
		backgroundColor: '#fff',
		padding: 5,
		alignSelf: 'stretch',
		textAlign: 'center'
	},
	label: {
		marginTop: margin-5,
		marginBottom: 5,
		alignItems: 'center',
	},
	textResult: {
		fontSize: 25
	}
});