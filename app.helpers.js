import React from 'react';
import { ImageBackground } from 'react-native';

export default class BackgroundImage extends React.Component {
  render() {
    return (
      <ImageBackground source={require('./background.png')} style={{
        flex: 1,
        width: null,
        height: null
      }}>
        {this.props.children}
      </ImageBackground>
    )
  }
}

/*
Reg expression in order to allow only numbers
*/

export function ValidateNumber(text) {
	return text.replace(/[^\d.-]/g, '');
}

/*
[a] Jakim stężeniem roztworu dysponujesz?
[b] Jakie stężenie % chcesz uzyskać po rozcieńczeniu wodą?
[c] Ile g roztworu chcesz otrzymać?
[d] Tyle g roztworu stężonego należy użyć
Schemat obliczeń:
d=(b*c)/a;
*/

export function SolutionDilitation(a, b, c) {
	const d = (Number(b) * Number(c) / Number(a)).toFixed(2)
	return isNaN(d) ? 0 : d
}

export function MgMl(a, b) {
  console.log('initial mg / ml: ' + a)
  console.log('target mg / ml: ' + b)
  const d = Number(a) / Number(b) - 1
  console.log('amount of solvent: ' + d)
  return isNaN(d) ? 0 : d == Infinity? 0 : d
}