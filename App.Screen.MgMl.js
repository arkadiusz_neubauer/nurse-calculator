import React from 'react';
import { Alert, Button, Text, TextInput, View, ScrollView } from 'react-native';
import styles from './app.style';
import strings from './app.strings';
import { ValidateNumber, MgMl } from './app.helpers'
import BackgroundImage from './app.helpers';

export default class MgMlScreen extends React.Component {
	static navigationOptions = {
		title: strings.titleMgMlCalculator
	}

	state = {
		initialMg: '',
		initialMl: '1',
		targetMgMl: '',
		result: 0
	}

	CalculateMgMl = (pInitialMg, pInitialMl, pTargetMgMl) => {
		console.log('mg: ' + pInitialMg)
		console.log('ml: ' + pInitialMl)

		let initialMgMl = Number(pInitialMg) / Number(pInitialMl)

		if (Number(pTargetMgMl) > Number(initialMgMl)) {
			if (Number(pInitialMg) > 0) {
				Alert.alert(
					`${strings.warning}`,
					`${strings.formatString(strings.mgMlWarning, initialMgMl.toFixed(2))}`,
					[
						{ text: 'OK' },
					],
					{ cancelable: false }
				)
			}
			pTargetMgMl = '' + initialMgMl.toFixed(2)
		}
		this.setState({
			initialMg: pInitialMg,
			initialMl: pInitialMl,
			targetMgMl: pTargetMgMl,
			result: MgMl(initialMgMl, pTargetMgMl).toFixed(2)
		});
	}

	render() {
		return (
			<BackgroundImage>
				<ScrollView contentContainerStyle={styles.scrollContainer}>
					<View style={styles.container} >

						<Text style={styles.label}>mg</Text>
						<TextInput
							style={styles.input}
							keyboardType={'numeric'}
							placeholder='mg'
							underlineColorAndroid='transparent'
							disableFullscreenUI={true}
							onChangeText={(value) => this.CalculateMgMl(ValidateNumber(value), this.state.initialMl, this.state.targetMgMl)}
							value={this.state.initialMg}
							maxLength={10}
						/>

						<Text style={styles.label}>ml</Text>
						<TextInput
							style={styles.input}
							keyboardType={'numeric'}
							placeholder='ml'
							underlineColorAndroid='transparent'
							disableFullscreenUI={true}
							onChangeText={(value) => this.CalculateMgMl(this.state.initialMg, ValidateNumber(value), this.state.targetMgMl)}
							value={this.state.initialMl}
							maxLength={10}
						/>

						<Text style={styles.label}>{strings.targetMgMlLabel}</Text>
						<TextInput
							style={styles.input}
							keyboardType={'numeric'}
							placeholder='mg / ml'
							underlineColorAndroid='transparent'
							disableFullscreenUI={true}
							onChangeText={(value) => this.CalculateMgMl(this.state.initialMg, this.state.initialMl, ValidateNumber(value))}
							value={this.state.targetMgMl}
							maxLength={10}
						/>

					</View>
					<View style={styles.sectionResult}>
						<Text>{strings.mgMlResultLabel}</Text>
						<Text style={styles.textResult}>{this.state.result} ml</Text>
					</View>
					<View style={{ margin: 20 }}>
						<Button
							title={strings.back}
							onPress={() => this.props.navigation.goBack()}
						/>
					</View>
				</ScrollView >
			</BackgroundImage>
		);
	}
}