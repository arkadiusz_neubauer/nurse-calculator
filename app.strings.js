import LocalizedStrings from 'react-native-localization';

export default new LocalizedStrings({
	en: {
		appName: "Nurse Calculator",
		warning: "Warning",
		back: "Go back",
		titleDilutingSolutions: "Diluting the x% solutions",
		initialSolutionConcentration: "What is the initial solution concentration? [%]",
		targetSolutionConcentration: "What is the solution concentration after diluting? [%]",
		targetSolutionAmount: "What's the target amount of the solution? [g]",
		targetSolutionConcentrationWarning: "Target solution concentration can't be bigger than {0} %",
		amountOfInitialSolution: "Amount g of initial solution to use:",
		amountOfWater: "Amount g of solvent to use:",
		credits: "For my beloved wife Paulina",
		targetSolutionHint: "3% enter: 3",
		sourceSolutionHint: "30% perchlorate enter: 30",
		titleMgMlCalculator: "mg / ml calculator",
		targetMgMlLabel: "Target mg / ml",
		mgMlResultLabel: "Amount of solvent:",
		mgMlWarning: "Target density can't be bigger {0} mg / ml"
	},
	pl: {
		appName: "Pomocnik Pielęgniarki",
		warning: "Uwaga",
		back: "Powrót",
		titleDilutingSolutions: "Rozcieńczanie roztworów x%",
		initialSolutionConcentration: "Jakim stężeniem roztworu dysponujesz? [%]",
		targetSolutionConcentration: "Jakie stężenie % chcesz uzyskać po rozcieńczeniu? [%]",
		targetSolutionAmount: "Ile g roztworu chcesz otrzymać? [g]",
		targetSolutionConcentrationWarning: "Stężenie po rozcieńczeniu nie może być większe niż {0} %",
		amountOfInitialSolution: "Tyle g roztworu stężonego należy użyć:",
		amountOfWater: "Tyle g rozpuszczalnika należy użyć:",
		credits: "Dla kochanej żony Pauliny",
		targetSolutionHint: "3% wpisz: 3",
		sourceSolutionHint: "30% r-r perhydrolu wpisz: 30",
		titleMgMlCalculator: "Przelicznik mg / ml",
		targetMgMlLabel: "Docelowa ilość mg / ml",
		mgMlResultLabel: "Ilość rozpuszczalnika:",
		mgMlWarning: "Docelowa gęstość nie może być większa niż {0} mg / ml"
	}
});