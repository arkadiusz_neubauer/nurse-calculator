import React from 'react';
import { Alert, Button, Text, TextInput, View, ScrollView } from 'react-native';
import styles from './app.style';
import strings from './app.strings';
import { ValidateNumber, SolutionDilitation } from './app.helpers'
import BackgroundImage from './app.helpers'

export default class DilutingSolutionScreen extends React.Component {
	static navigationOptions = {
		title: strings.titleDilutingSolutions
	}
	state = {
		initialConcentration: '',
		targetConcentration: '',
		targetSolution: '',
		solutionDilitation: 0,
		amountOfWater: 0
	};

	CalculateSolution = (pInitialConcentration, pTargetConcentration, pTargetSolution) => {
		if (Number(pTargetConcentration) > Number(pInitialConcentration)) {
			if (Number(pInitialConcentration) > 0) {
				Alert.alert(
					`${strings.warning}`,
					`${strings.formatString(strings.targetSolutionConcentrationWarning, pInitialConcentration)}`,
					[
						{ text: 'OK' },
					],
					{ cancelable: false }
				)
			}
			pTargetConcentration = pInitialConcentration
		}

		const pSolutionDilitation = SolutionDilitation(pInitialConcentration, pTargetConcentration, pTargetSolution)

		this.setState({
			initialConcentration: pInitialConcentration,
			targetConcentration: pTargetConcentration,
			targetSolution: pTargetSolution,
			solutionDilitation: pSolutionDilitation,
			amountOfWater: (pTargetSolution - pSolutionDilitation).toFixed(2)
		});
	}

	render() {
		return (
			<BackgroundImage>
				<ScrollView contentContainerStyle={styles.scrollContainer}>
					<View style={styles.container} >
						<Text style={styles.label}>{strings.initialSolutionConcentration}</Text>
						<TextInput
							style={styles.input}
							keyboardType={'numeric'}
							placeholder={strings.sourceSolutionHint}
							underlineColorAndroid='transparent'
							disableFullscreenUI={true}
							onChangeText={(value) => this.CalculateSolution(ValidateNumber(value), this.state.targetConcentration, this.state.targetSolution)}
							value={this.state.initialConcentration}
							maxLength={10}
						/>
						<Text style={styles.label}>{strings.targetSolutionConcentration}</Text>
						<TextInput
							style={styles.input}
							keyboardType={'numeric'}
							placeholder={strings.targetSolutionHint}
							underlineColorAndroid='transparent'
							disableFullscreenUI={true}
							onChangeText={(value) => this.CalculateSolution(this.state.initialConcentration, ValidateNumber(value), this.state.targetSolution)}
							value={this.state.targetConcentration}
							maxLength={10}
						/>
						<Text style={styles.label}>{strings.targetSolutionAmount}</Text>
						<TextInput
							style={styles.input}
							keyboardType={'numeric'}
							placeholder='100g'
							underlineColorAndroid='transparent'
							disableFullscreenUI={true}
							onChangeText={(value) => this.CalculateSolution(this.state.initialConcentration, this.state.targetConcentration, ValidateNumber(value))}
							value={this.state.targetSolution}
							maxLength={10}
						/>
					</View>
					<View style={styles.sectionResult}>
						<Text>{strings.amountOfInitialSolution}</Text>
						<Text style={styles.textResult}>{this.state.solutionDilitation} g</Text>
						<Text>{strings.amountOfWater}</Text>
						<Text style={styles.textResult}>{this.state.amountOfWater} g</Text>
					</View>
					<View style={{ margin: 20 }}>
						<Button
							title={strings.back}
							onPress={() => this.props.navigation.goBack()}
						/>
					</View>
				</ScrollView >
			</BackgroundImage>
		);
	}
}