import React from 'react';
import { Button, Text, Image, View, ScrollView } from 'react-native';
import styles from './app.style.js';
import strings from './app.strings.js';
import BackgroundImage from './app.helpers';

export default class InfoScreen extends React.Component {
	static navigationOptions = {
		title: 'Info',
	};
	render() {
		return (
			<BackgroundImage>
				<ScrollView contentContainerStyle={styles.scrollContainer}>
					<View style={{ flex: 1, alignItems: 'center', justifyContent: 'center', padding: 20 }}>
						{true &&
							<Image style={{ width: 250, height: 250, }} source={require('./paulna-filter.jpg')} />
						}
						<Text style={{ fontSize: 25, textShadowOffset: { width: 1, height: 2, }, textShadowColor: '#E65345' }}>{strings.credits}</Text>
						<Image style={{ width: 200, height: 200, }} source={require('./heart.png')} />
						<Button
							title={strings.back}
							onPress={() => this.props.navigation.goBack()}
						/>
					</View>
				</ScrollView>
			</BackgroundImage>
		);
	}
}