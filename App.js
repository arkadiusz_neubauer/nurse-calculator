import React from 'react';
import {
	Button,
	Text,
	Image,
	View
} from 'react-native';
import { StackNavigator } from 'react-navigation';
import styles from './app.style';
import strings from './app.strings';
import BackgroundImage from './app.helpers';
import DilutingSolutionScreen from './App.Screen.DilutingSolution';
import MgMlScreen from './App.Screen.MgMl';
import InfoScreen from './App.Screen.Info';

// -----------------------------------------------------------------------

class HomeScreen extends React.Component {
	static navigationOptions = ({ navigation }) => {
		const params = navigation.state.params || {};
		return {
			headerTitle: <LogoTitle />,
			headerRight: (
				<Button
					onPress={() => navigation.navigate('Info')}
					title='Info'
				/>
			),
		}
	};
	render() {
		return (
			<BackgroundImage>
				<View style={{
					flex: 1,
					flexDirection: 'column',
					justifyContent: 'center',
					alignItems: 'center',
				}}>
					<View style={{ marginBottom: 20 }} >
						<Button
							title={strings.titleDilutingSolutions}
							onPress={() => this.props.navigation.navigate('DilutingSolution')}
						/>
					</View>
					<View>
						<Button
							title={strings.titleMgMlCalculator}
							onPress={() => this.props.navigation.navigate('MgMl')}
						/>
					</View>
				</View>
			</BackgroundImage>
		);
	}
}
// -----------------------------------------------------------------------
class LogoTitle extends React.Component {
	render() {
		return (
			<View style={{ flex: 1, flexDirection: 'row', alignItems: 'center' }}>
				<Image
					source={require('./nurse.png')}
					style={{ width: 42, height: 42 }}
				/>
				<Text style={{ fontSize: 20, marginLeft: 10 }}>{strings.appName}</Text>
			</View>
		);
	}
}
// -----------------------------------------------------------------------
const RootStack = StackNavigator(
	{
		Home: {
			screen: HomeScreen,
		},
		DilutingSolution: {
			screen: DilutingSolutionScreen
		},
		MgMl: {
			screen: MgMlScreen,
		},
		Info: {
			screen: InfoScreen,
		}
	},
	{
		initialRouteName: 'Home',
		/* The header config from HomeScreen is now here */
		navigationOptions: {
			headerStyle: {
				backgroundColor: 'skyblue',
				paddingLeft: 10,
				paddingRight: 10,
			},
			headerTintColor: '#000',
			headerTitleStyle: {
				fontWeight: 'bold',
			},
		},
	}
);

export default class App extends React.Component {
	render() {
		return <RootStack />;
	}
}